//
//  XLotoMensageiroDelegate.swift
//  XLotoMensageiro
//
//  Created by Diego Ferreira da Silva on 05/11/16.
//  Copyright © 2016 Steve Kite. All rights reserved.
//

import Foundation
import ActorSDK

/// Default empty implementation of SDK Delegate
open class XLotoMensageiroDelegate: NSObject, ActorSDKDelegate {
    
    open func actorControllerForAuthStart() -> UIViewController? {
        return nil
    }
    
    open func actorControllerForStart() -> UIViewController? {
        return nil
    }
    
    open func actorControllerForUser(_ uid: Int) -> AAViewController? {
        return nil
    }
    
    open func actorControllerForGroup(_ gid: Int) -> AAViewController? {
        return nil
    }
    
    open func actorControllerForConversation(_ peer: ACPeer) -> UIViewController? {
        return nil
    }
    
    //Retorna o controllador para a primeira aba
    //controlador padrao AAContactsViewController
    open func actorControllerForContacts() -> UIViewController? {
        return nil
    }
    
    open func actorControllerForDialogs() -> UIViewController? {
        return nil
    }
    
    open func actiorControllerForCompose() -> UIViewController? {
        return nil
    }
    
    open func actorControllerForSettings() -> UIViewController? {
        return nil
    }
    
    open func actorRootControllers() -> [UIViewController]? {
        var mainNavigations = [UIViewController]()
        
        ////////////////////////////////////
        // Contacts
        ////////////////////////////////////
        
        if let contactsController = self.actorControllerForContacts() {
            mainNavigations.append(contactsController)
        } else {
            mainNavigations.append(AAContactsViewController())
        }
        
        ////////////////////////////////////
        // Recent dialogs
        ////////////////////////////////////
        
        if let recentDialogs = self.actorControllerForDialogs() {
            mainNavigations.append(recentDialogs)
        } else {
            mainNavigations.append(AARecentViewController())
        }
        
        //Enquetes
        //mainNavigations.append(XLEnquetesController())
        
        ////////////////////////////////////
        // Settings
        ////////////////////////////////////
        
        if let settingsController = self.actorControllerForSettings() {
            mainNavigations.append(settingsController)
        } else {
            mainNavigations.append(AASettingsViewController())
        }
        
      
        
        return mainNavigations
    }
    
    open func actorRootInitialControllerIndex() -> Int? {
        return nil
    }
    
    open func actorConfigureBubbleLayouters(_ builtIn: [AABubbleLayouter]) -> [AABubbleLayouter] {
        return builtIn
    }
    
    open func actorControllerAfterLogIn() -> UIViewController? {
        return nil
    }
    
    open func actorConversationCustomAttachMenu(_ controller: UIViewController) -> Bool {
        return false
    }
    
    open func actorSettingsHeaderDidCreated(_ controller: AASettingsViewController, section: AAManagedSection) {
        
    }
    
    open func actorSettingsConfigurationWillCreated(_ controller: AASettingsViewController, section: AAManagedSection) {
        
    }
    
    open func actorSettingsConfigurationDidCreated(_ controller: AASettingsViewController, section: AAManagedSection) {
        
    }
    
    open func actorSettingsSupportWillCreated(_ controller: AASettingsViewController, section: AAManagedSection) {
        
    }
    
    open func actorSettingsSupportDidCreated(_ controller: AASettingsViewController, section: AAManagedSection) {
        
    }
    
    open func showStickersButton() -> Bool{
        return false
    }
    
    open func useOnClientPrivacy() -> Bool{
        return false
    }
}
