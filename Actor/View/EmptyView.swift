//
//  EmptyView.swift
//  XLotoMensageiro
//
//  Created by Diego Ferreira da Silva on 16/01/17.
//  Copyright © 2017 Steve Kite. All rights reserved.
//

import UIKit


protocol EmptyViewProtocol {
    func onAction()
}

class EmptyView: UIView {
    
    private var label:UILabel!
    private var button:UIButton!
    public var delegate:EmptyViewProtocol!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }

}
