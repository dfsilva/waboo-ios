//
//  Copyright (c) 2014-2015 Actor LLC. <https://actor.im>
//

import UIKit
import ActorSDK

@UIApplicationMain
class AppDelegate: ActorApplicationDelegate {
    
    var window:UIWindow?

    override init() {
        super.init()
        
        ActorSDK.sharedActor().appName = "XLoto Mensageiro"
        // Invite URL configuration
        ActorSDK.sharedActor().inviteUrlHost = "msg.xloto.com.br"
        ActorSDK.sharedActor().inviteUrlScheme = "xloto"
        
        // Support Information configuration
        ActorSDK.sharedActor().supportHomepage = "https://xloto.com.br"
        ActorSDK.sharedActor().supportTwitter = nil
        ActorSDK.sharedActor().supportAccount = "5555555555555"
        //ActorSDK.sharedActor().supportEmail = "diegosiuniube@gmail.com"
        //ActorSDK.sharedActor().supportActivationEmail = "activation@actor.im"
 
        
        // Invitations to app
        ActorSDK.sharedActor().inviteUrl = "http://xloto.com.br"
        ActorSDK.sharedActor().invitePrefix = "https://im.xloto.com.br/join/"
        ActorSDK.sharedActor().invitePrefixShort = "im.xloto.com.br/join/"
        
        // Push Configuration
        // Enter Here your push id
        // ActorSDK.sharedActor().push
        // Enable Push Notifications only after log in
         ActorSDK.sharedActor().autoPushMode = .afterLogin
        ActorSDK.sharedActor().apiPushId = 868547
        
        ActorSDK.sharedActor().enableVideoCalls = false;
        ActorSDK.sharedActor().enableCalls = true;
        
        
        // Styling of app
        
        let style = ActorSDK.sharedActor().style
        
        // Default Status Bar style
         style.vcStatusBarStyle = .lightContent
        
        // Navigation colors
         style.navigationBgColor = UIColor(rgb: 0xA43436)
         style.dialogStatusActive = UIColor(rgb: 0xff5882)
         style.welcomeBgColor = UIColor(rgb: 0xA43436)
        
         ActorSDK.sharedActor().style.welcomeSignupTextColor = UIColor(rgb: 0xA43436)
         ActorSDK.sharedActor().style.nextBarColor = UIColor(rgb: 0xA43436)
        
         style.navigationTintColor = UIColor.white
         style.navigationTitleColor = UIColor.white
         style.navigationSubtitleColor = UIColor.white.alpha(0.64)
         style.navigationSubtitleActiveColor = UIColor.white
        // style.navigationHairlineHidden = true
        
        // Full screen placeholder. Set here value that matches UINavigationBar color
         style.placeholderBgColor = UIColor(rgb: 0x528dbe)
        
        // Override User's online/offline statuses in navigation color
         style.userOfflineNavigationColor = UIColor.white.alpha(0.64)
         style.userOnlineNavigationColor = UIColor.white
        
        // Override search status bar style
         style.searchStatusBarStyle = .default
         style.welcomeLogo = UIImage.bundled("welcome_logo")
        
         ActorSDK.sharedActor().endpoints = ["tcp://192.168.31.226:9070"]
        
        ActorSDK.sharedActor().delegate = XLotoMensageiroDelegate()
        
        ActorSDK.sharedActor().authStrategy = .phoneOnly

        ActorSDK.sharedActor().autoJoinGroups = ["canalxloto"]
        
        // Creating Actor
        ActorSDK.sharedActor().createActor()
    }
    
    open override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) -> Bool {
        super.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        ActorSDK.sharedActor().presentMessengerInNewWindow()
        
        return true
    }
}
