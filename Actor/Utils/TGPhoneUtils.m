#import "TGPhoneUtils.h"

#import "XLRMPhoneFormat.h"

//#import <ActorSDK/RMPhoneFormat.h>

@implementation TGPhoneUtils

+ (BOOL)formatPhoneXloto:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)phoneNumber
{
    
    int stringLength = (int)phoneNumber.length;
    unichar replacementCharacters[stringLength];
    int filteredLength = 0;
    
    for (int i = 0; i < stringLength; i++)
    {
        unichar c = [phoneNumber characterAtIndex:i];
        if (c >= '0' && c <= '9')
        replacementCharacters[filteredLength++] = c;
    }
    
    NSString *replacementString = [[NSString alloc] initWithCharacters:replacementCharacters length:filteredLength];
    
    unichar rawNewString[replacementString.length];
    int rawNewStringLength = 0;
    
    int replacementLength = (int)replacementString.length;
    for (int i = 0; i < replacementLength; i++)
    {
        unichar c = [replacementString characterAtIndex:i];
        if ((c >= '0' && c <= '9'))
        rawNewString[rawNewStringLength++] = c;
    }
    
    NSString *string = [[NSString alloc] initWithCharacters:rawNewString length:rawNewStringLength];
    
    NSMutableString *rawText = [[NSMutableString alloc] initWithCapacity:16];
    NSString *currentText = textField.text;
    int length = (int)currentText.length;
    
    int originalLocation = (int)range.location;
    int originalEndLocation = (int)range.location + (int)range.length;
    int endLocation = originalEndLocation;
    
    for (int i = 0; i < length; i++)
    {
        unichar c = [currentText characterAtIndex:i];
        if ((c >= '0' && c <= '9'))
        [rawText appendString:[[NSString alloc] initWithCharacters:&c length:1]];
        else
        {
            if (originalLocation > i)
            {
                if (range.location > 0)
                range.location--;
            }
            
            if (originalEndLocation > i)
            endLocation--;
        }
    }
    
    int newLength = endLocation - (int)range.location;
    if (newLength == 0 && range.length == 1 && range.location > 0)
    {
        range.location--;
        newLength = 1;
    }
    if (newLength < 0)
    return false;
    
    range.length = newLength;
    
    @try
    {
        int caretPosition = (int)range.location + (int)string.length;
        
        [rawText replaceCharactersInRange:range withString:string];
        
        NSString *countryCodeText = @"+55";
        
        NSString *formattedText = [TGPhoneUtils formatPhone:[[NSString alloc] initWithFormat:@"%@%@", countryCodeText, rawText] forceInternational:false];
        if (countryCodeText.length > 1)
        {
            int i = 0;
            int j = 0;
            while (i < (int)formattedText.length && j < (int)countryCodeText.length)
            {
                unichar c1 = [formattedText characterAtIndex:i];
                unichar c2 = [countryCodeText characterAtIndex:j];
                if (c1 == c2)
                j++;
                i++;
            }
            
            formattedText = [formattedText substringFromIndex:i];
            
            i = 0;
            while (i < (int)formattedText.length)
            {
                unichar c = [formattedText characterAtIndex:i];
                if ((c == ')' && i != 0) || c == '(' || (c >= '0' && c <= '9'))
                break;
                
                i++;
            }
    
           formattedText = [TGPhoneUtils filterPhoneText:[formattedText substringFromIndex:i]];
        }
        
        int formattedTextLength = (int)formattedText.length;
        int rawTextLength = (int)rawText.length;
        
        int newCaretPosition = caretPosition;
        
        for (int j = 0, k = 0; j < formattedTextLength && k < rawTextLength; )
        {
            unichar c1 = [formattedText characterAtIndex:j];
            unichar c2 = [rawText characterAtIndex:k];
            if (c1 != c2)
            newCaretPosition++;
            else
            k++;
            
            if (k == caretPosition)
            {
                break;
            }
            
            j++;
        }
        
        textField.text = formattedText;
        
        if (caretPosition >= (int)textField.text.length)
        caretPosition = (int)textField.text.length;
        
        UITextPosition *startPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCaretPosition];
        UITextPosition *endPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCaretPosition];
        UITextRange *selection = [textField textRangeFromPosition:startPosition toPosition:endPosition];
        textField.selectedTextRange = selection;
    }
    @catch (NSException *e)
    {
        NSLog(@"%@", e);
    }
    
    return false;
    
}
    
+ (NSString *)filterPhoneText:(NSString *)text
{
        int i = 0;
        while (i < (int)text.length)
        {
            unichar c = [text characterAtIndex:i];
            if ((c >= '0' && c <= '9'))
            return text;
            
            i++;
        }
        
        return @"";
}

+ (NSString *)formatPhone:(NSString *)phone forceInternational:(bool)forceInternational
{
    if (phone == nil)
        return @"";
    
    return [[XLRMPhoneFormat instance] format:phone implicitPlus:forceInternational];
}

+ (NSString *)formatPhoneUrl:(NSString *)phone
{
    if (phone == nil)
        return @"";
    
    unichar cleanPhone[phone.length];
    int cleanPhoneLength = 0;
    
    int length = (int)phone.length;
    for (int i = 0; i < length; i++)
    {
        unichar c = [phone characterAtIndex:i];
        if (!(c == ' ' || c == '(' || c == ')' || c == '-'))
            cleanPhone[cleanPhoneLength++] = c;
    }
    
    return [[NSString alloc] initWithCharacters:cleanPhone length:cleanPhoneLength];
}

+ (NSString *)cleanPhone:(NSString *)phone
{
    if (phone.length == 0)
        return @"";
    
    char buf[phone.length];
    int bufPtr = 0;
    
    int length = (int)phone.length;
    for (int i = 0; i < length; i++)
    {
        unichar c = [phone characterAtIndex:i];
        if (c >= '0' && c <= '9')
        {
            buf[bufPtr++] = (char)c;
        }
    }
    
    return [[NSString alloc] initWithBytes:buf length:bufPtr encoding:NSUTF8StringEncoding];
}

+ (NSString *)cleanInternationalPhone:(NSString *)phone forceInternational:(bool)forceInternational
{
    if (phone.length == 0)
        return @"";
    
    char buf[phone.length];
    int bufPtr = 0;
    
    bool hadPlus = false;
    int length = (int)phone.length;
    for (int i = 0; i < length; i++)
    {
        unichar c = [phone characterAtIndex:i];
        if ((c >= '0' && c <= '9') || (c == '+' && !hadPlus))
        {
            buf[bufPtr++] = (char)c;
            if (c == '+')
                hadPlus = true;
        }
    }
    
    NSString *result = [[NSString alloc] initWithBytes:buf length:bufPtr encoding:NSUTF8StringEncoding];
    if (forceInternational && bufPtr != 0 && buf[0] != '+')
        result = [[NSString alloc] initWithFormat:@"+%@", result];
    return result;
}

@end
