//
//  XLotoColors.swift
//  XLotoMensageiro
//
//  Created by Diego Ferreira da Silva on 03/01/17.
//  Copyright © 2017 Steve Kite. All rights reserved.
//

import Foundation

open class XLotoColors {
        
    public static let XLOTO_GRAY_COLORS = [UIColor(rgb: 0x8F8F8F), UIColor(rgb: 0x999999), UIColor(rgb: 0xA3A3A3), UIColor(rgb: 0xADADAD)]
    
}
