//
//  DateUtils.swift
//  XLotoMensageiro
//
//  Created by Diego Ferreira da Silva on 07/01/17.
//  Copyright © 2017 Steve Kite. All rights reserved.
//

import Foundation


enum DateFormats : String {
    case dateHourMinutes = "dd/MM/yyyy HH:mm"
    case dateHourMinutesSeconds = "dd/MM/yyyy HH:mm:ss"
    case date = "dd/MM/yyyy"
    case hour = "HH:mm"
}

class DateUtils {
 
    public static func from(string:String, format:DateFormats) -> Date{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format.rawValue

        return dateFormater.date(from: string)!
    }
    
    public static func format(date:Date, format:DateFormats) -> String{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format.rawValue
        return dateFormater.string(from: date)
    }
    
    public static func getDiaFormatadoFalado(data:Date!) -> String {
        if (data != nil) {
            if (DateUtils.getDaysDiff(start: data, end: Date()) == 0) {
                return "hoje"
            } else {
                return "dia " + DateUtils.format(date: data, format: .date)
            }
        }
        return "";
    }
    
    public static func getHoraFormatadaFalada(data:Date!) -> String {
        if (data != nil) {
            return "as \(DateUtils.format(date: data, format: .hour)) horas"
        }
        return "";
    }
    
    public static func getDaysDiff(start:Date, end:Date) -> Int {
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
    
        let components = calendar.dateComponents([.day], from: date1, to: date2)
    
        return components.day!
    }

}
